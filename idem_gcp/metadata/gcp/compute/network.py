"""Metadata module for managing Networks."""

PATH = "projects/{project}/global/networks/{network}"

NATIVE_RESOURCE_TYPE = "compute.networks"
