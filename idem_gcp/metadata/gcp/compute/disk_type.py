"""Metadata module for managing Disk Types."""

PATH = "projects/{project}/zones/{zone}/diskTypes/{diskType}"

NATIVE_RESOURCE_TYPE = "compute.disk_types"
