"""Metadata module for managing Disks."""

PATH = "projects/{project}/zones/{zone}/disks/{disk}"

NATIVE_RESOURCE_TYPE = "compute.disks"
