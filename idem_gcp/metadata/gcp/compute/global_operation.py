"""Metadata module for managing Global Operations."""

PATH = "projects/{project}/global/operations/{operation}"

NATIVE_RESOURCE_TYPE = "compute.global_operations"
