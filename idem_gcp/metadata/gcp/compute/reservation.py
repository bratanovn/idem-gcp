"""Metadata module for managing Reservations."""

PATH = "projects/{project}/zones/{zone}/reservations/{reservation}"

NATIVE_RESOURCE_TYPE = "compute.reservations"
