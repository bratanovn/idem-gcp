"""Metadata module for managing Machine Images."""

PATH = "projects/{project}/global/machineImages/{machineImage}"

NATIVE_RESOURCE_TYPE = "compute.machine_images"
