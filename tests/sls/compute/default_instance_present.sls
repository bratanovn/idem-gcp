idem-fixture-instance-test-{timestamp}:
  gcp.compute.instance.present:
  - project: tango-gcp
  - machine_type: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small
  - zone: us-central1-a
  - can_ip_forward: false
  - status: RUNNING
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        network_tier: PREMIUM
        set_public_ptr: false
        type: ONE_TO_ONE_NAT
      kind: compute#networkInterface
      name: nic0
      network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default
      stack_type: IPV4_ONLY
      subnetwork: https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default
  - disks:
    - kind: compute#attachedDisk
      auto_delete: true
      boot: true
      device_name: idem-fixture-instance-test-{timestamp}
      initialize_params:
        - disk_size_gb: "10"
          disk_type: "projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced"
          source_image: "projects/debian-cloud/global/images/debian-11-bullseye-v20221102"
      licenses:
        - https://www.googleapis.com/compute/v1/projects/debian-cloud/global/licenses/debian-11-bullseye
      guest_os_features:
        - type: UEFI_COMPATIBLE
        - type: VIRTIO_SCSI_MULTIQUEUE
        - type: GVNIC
      mode: READ_WRITE
      type: PERSISTENT
      index: 0
      interface: SCSI
      disk_size_gb: "10"
      architecture: X86_64
  - scheduling:
      automatic_restart: true
      on_host_maintenance: MIGRATE
      preemptible: false
      provisioning_model: STANDARD
  - deletion_protection: false
  - tags:
      items:
        - test1
        - test2
        - test3
  - metadata:
      kind: compute#metadata
      items:
       - key: sample_metadata_key
         value: sample_metadata_value
  - shielded_instance_config:
     enable_secure_boot: false
     enable_vtpm: true
     enable_integrity_monitoring: true
  - shielded_instance_integrity_policy:
      update_auto_learn_policy: true
