{name}:
  gcp.compute.disk.present:
  - zone: us-central1-a
  - project: tango-gcp
  - type: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: {size_gb}
  - guest_os_features:
    - type: VIRTIO_SCSI_MULTIQUEUE
    - type: SEV_CAPABLE
    - type: UEFI_COMPATIBLE
    - type: GVNIC
  - resource_policies: {resource_policies}
  - labels: {labels}
  - label_fingerprint: {label_fingerprint}
