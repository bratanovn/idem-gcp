from typing import Dict
from typing import List

import yaml
from pytest_idem import runner


def call_present_from_properties(
    hub,
    idem_cli,
    resource_type: str,
    present_state_properties: Dict,
    test: bool = False,
    additional_kwargs: List[str] = None,
) -> Dict:
    name = present_state_properties["name"]
    present_state_str = yaml.safe_dump(
        {
            name: {
                f"gcp.{resource_type}.present": [
                    {k: v} for k, v in present_state_properties.items()
                ]
            }
        }
    )

    present_state_ret = hub.tool.utils.run_idem_state(
        idem_cli, present_state_str, test, additional_kwargs
    )
    return present_state_ret[f"gcp.{resource_type}_|-{name}_|-{name}_|-present"]


def call_present_from_sls(
    hub,
    idem_cli,
    present_state_sls_str: str,
    test: bool = False,
) -> Dict:
    present_state_dict = yaml.safe_load(present_state_sls_str)
    name = list(present_state_dict.keys())[0]
    gcp_resource_type = list(present_state_dict[name].keys())[0].replace(".present", "")

    present_state_ret = hub.tool.utils.run_idem_state(
        idem_cli, present_state_sls_str, test
    )
    return present_state_ret[f"{gcp_resource_type}_|-{name}_|-{name}_|-present"]


def call_present(
    hub,
    idem_cli,
    resource_type: str,
    present_state_properties: Dict,
    test: bool = False,
) -> Dict:
    name = present_state_properties["name"]
    present_format = {
        name: {
            f"gcp.{resource_type}.present": [
                {k: v} for k, v in present_state_properties.items()
            ]
        }
    }
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(yaml.dump(present_format))
        args = ["--acct-profile=test_development_idem_gcp"]
        if test:
            args.append("--test")
        state_ret = idem_cli(
            "state",
            fh,
            *args,
            check=True,
        ).json

        return state_ret[f"gcp.{resource_type}_|-{name}_|-{name}_|-present"]


def call_absent(
    hub,
    idem_cli,
    resource_type: str,
    name: str,
    resource_id: str,
    zone: str = None,
    project: str = None,
    test: bool = False,
    additional_kwargs: List[str] = None,
) -> Dict:
    if resource_id:
        absent_state_str = f"""
            {name}:
              gcp.{resource_type}.absent:
              - resource_id: {resource_id}
            """
    elif name and zone and project:
        absent_state_str = f"""
            {name}:
              gcp.{resource_type}.absent:
              - name: {name}
              - zone: {zone}
              - project: {project}
            """
    else:
        absent_state_str = f"""
            {name}:
              gcp.{resource_type}.absent:
              - name: {name}
            """

    absent_state_ret = hub.tool.utils.run_idem_state(
        idem_cli, absent_state_str, test, additional_kwargs
    )
    return absent_state_ret[f"gcp.{resource_type}_|-{name}_|-{name}_|-absent"]


def run_idem_state(
    hub, idem_cli, state: str, test: bool = False, additional_args: List = []
):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(state)
        args = ["--acct-profile=test_development_idem_gcp"]
        if additional_args:
            for item in additional_args:
                args.append(item)
        if test:
            args.append("--test")
        return idem_cli(
            "state",
            fh,
            *args,
            check=True,
        ).json
