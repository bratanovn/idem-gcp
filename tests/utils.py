"""Creates and deletes GCP instance using the

Usage pattern taken from: https://github.com/GoogleCloudPlatform/python-docs-samples/blob/81a8413528860ff368a3e9f3b7f4e3665588d07f/compute/api/create_instance.py

Running the methods requires the GOOGLE_APPLICATION_CREDENTIALS environment variable
to contain the file path to a service account credentials JSON file
- as obtained by the GCP console
"""
import time

import googleapiclient


def create_external_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        source_disk_image = (
            "projects/debian-cloud/global/images/debian-11-bullseye-v20221102"
        )
        config = {
            "name": name,
            "machineType": "https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small",
            "disks": [
                {
                    "boot": True,
                    "autoDelete": True,
                    "initializeParams": {
                        "sourceImage": source_disk_image,
                    },
                }
            ],
            "networkInterfaces": [
                {
                    "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
                    "accessConfigs": [
                        {"type": "ONE_TO_ONE_NAT", "name": "External NAT"}
                    ],
                }
            ],
        }
        return (
            compute_service.instances()
            .insert(project=project, zone=zone, body=config)
            .execute()
        )


def stop_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        return (
            compute_service.instances()
            .stop(project=project, zone=zone, instance=name)
            .execute()
        )


def delete_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        return (
            compute_service.instances()
            .delete(project=project, zone=zone, instance=name)
            .execute()
        )


def wait_for_operation(ctx, project, zone, operation):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        while True:
            result = (
                compute_service.zoneOperations()
                .get(project=project, zone=zone, operation=operation["id"])
                .execute()
            )

            if result["status"] == "DONE":
                if "error" in result:
                    raise Exception(result["error"])
                return result

            time.sleep(5)
