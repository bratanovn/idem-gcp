import pytest


@pytest.mark.asyncio
async def test_hub_fixture_load(hub):
    assert hub.tool.gcp
    assert hub.states.gcp
    assert hub.exec.gcp


@pytest.mark.asyncio
async def test_mock_hub_fixture_load(mock_hub):
    assert mock_hub.tool.gcp
    assert mock_hub.states.gcp
    assert mock_hub.exec.gcp
