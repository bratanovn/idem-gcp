from typing import Dict

import pytest


@pytest.mark.parametrize(
    "new_labels,old_labels",
    [
        ({"lbl1": "v1_new", "lbl2": "v2"}, {"lbl1": "v1_old", "lbl3": "v3"}),
        (
            {"lbl1": "v1_new", "lbl2": "v2", "lbl4": "v4"},
            {"lbl1": "v1_old", "lbl3": "v3", "lbl4": "v4"},
        ),
        ({"lbl1": "v1_new", "lbl2": "v2", "lbl3": "v3"}, None),
        (
            None,
            {"lbl1": "v1_old", "lbl2": "v2", "lbl3": "v3"},
        ),
        (None, None),
    ],
)
def test_merge_labels(hub, new_labels: Dict[str, str], old_labels: Dict[str, str]):
    merge_result: Dict[str, str] = hub.tool.gcp.cloudkms.patch.merge_labels(
        new_labels, old_labels
    )
    if new_labels:
        for k in new_labels.keys():
            assert (
                k in merge_result.keys() and new_labels[k] == merge_result[k]
            ), "New label merge mismatch!"

    if old_labels:
        for k in old_labels.keys():
            assert k in merge_result.keys() and (
                old_labels[k] == merge_result[k] or new_labels[k] == merge_result[k]
            ), "Old label merge mismatch!"
