from typing import Dict

import yaml

_SLS_1 = """
idem-gcp-vm:
  gcp.compute.instance.present:
  - name: idem-gcp-vm-2
  - project: project-name
  - description: 'Description'
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type: ONE_TO_ONE_NAT
      - kind: compute#accessConfig2
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type: ONE_TO_ONE_NAT
      fingerprint: GpxokVYsg7M=
      kind: compute#networkInterface
      name: nic0
      network: https://www.googleapis.com/compute/v1/projects/project-name/global/networks/default
      network_ip: 10.128.15.203
      stack_type: IPV4_ONLY
      subnetwork: https://www.googleapis.com/compute/v1/projects/project-name/regions/us-central1/subnetworks/default
  - label_fingerprint: 42WmSpB8rSM=
  - fingerprint: 0WBA6Ipp35U=
"""

_SLS_2 = """
idem-gcp-vm:
  gcp.compute.instance.present:
  - name: idem-gcp-vm-2
  - description: 'New Description'
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type: ONE_TO_ONE_NAT
      - kind: compute#accessConfig2
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type: ONE_TO_ONE_NAT
      fingerprint: GpxokVYsg7M=
      kind: compute#networkInterface
      name: nic0
      network: https://www.googleapis.com/compute/v1/projects/project-name/global/networks/default
      network_ip: 10.128.15.203
      stack_type: IPV4_ONLY
      subnetwork: https://www.googleapis.com/compute/v1/projects/project-name/regions/us-central1/subnetworks/default
  - label_fingerprint: 52WmSpB8rSM=
  - fingerprint: 1WBA6Ipp35U=
  - key_revocation_action_type: NONE
"""


def test_compare_states(hub):
    s1 = yaml.load(_SLS_1, Loader=yaml.Loader)
    l1 = s1["idem-gcp-vm"]["gcp.compute.instance.present"]
    s1 = {}
    for l0 in l1:
        s1.update(**l0)

    s2 = yaml.load(_SLS_2, Loader=yaml.Loader)
    l2 = s2["idem-gcp-vm"]["gcp.compute.instance.present"]
    s2 = {}
    for l0 in l2:
        s2.update(**l0)

    changes: Dict = hub.tool.gcp.utils.compare_states(s1, s2, "compute.instance")
    assert changes is not None
    assert changes.keys() == {"values_changed", "dictionary_item_added"}
    assert changes["values_changed"] == {
        "root['description']": {
            "new_value": "New Description",
            "old_value": "Description",
        }
    }
    assert changes["dictionary_item_added"] == {"root['key_revocation_action_type']"}
