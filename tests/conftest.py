import pathlib
import sys
import unittest.mock as mock

import pytest


@pytest.fixture(scope="session")
def code_dir() -> pathlib.Path:
    return pathlib.Path(__file__).parent.parent.absolute()


@pytest.fixture(scope="session", autouse=True)
def base_path(code_dir):
    base = pathlib.Path(__file__).parent.parent.absolute()
    sys_path = [str(base)]
    for p in sys.path:
        if p not in sys_path:
            sys_path.append(p)
    tpath_dir = str(code_dir / "tests" / "tpath")
    with mock.patch("sys.path", [tpath_dir] + sys.path):
        yield
