import asyncio
from typing import Any
from typing import Dict
from typing import List

import dict_tools
import pop
import pytest
import pytest_asyncio


# ================================================================================
# pop fixtures
# ================================================================================


@pytest.fixture(scope="module", name="acct_data")
def acct_data(ctx):
    """
    acct_data that can be used in running simple yaml blocks
    """
    yield {"profiles": {"gcp": {"default": ctx.acct}}}


@pytest.fixture(scope="module", autouse=True)
def acct_subs() -> List[str]:
    return ["gcp"]


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_gcp"


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="hub")
def integration_hub(tests_dir, event_loop):

    hub = pop.hub.Hub()
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.config.load(["idem", "acct", "idem_gcp"], cli="idem", parse_cli=False)

    hub.idem.RUNS = {"test": {}}
    yield hub


@pytest_asyncio.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(
        run_name="test",
        test=False,
        tag="fake_|-test_|-tag",
        old_state={},
    )

    if not hub.OPT.acct.acct_file:
        pytest.skip(
            "Missing acct_file.  "
            "Add ACCT_FILE to your environment with the path to your encrypted credentials file"
        )
    if not hub.OPT.acct.acct_key:
        pytest.skip(
            "Missing acct_key.  "
            "Add ACCT_KEY to your environment with the fernet key to decrypt your credentials file"
        )

    # Add the profile to the account
    await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    if not hub.acct.UNLOCKED:
        pytest.skip(f"acct could not unlock {hub.OPT.acct.acct_file}")

    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)

    yield ctx


# --------------------------------------------------------------
