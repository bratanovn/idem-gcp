import copy
import time
import uuid
from collections import ChainMap
from typing import Any
from typing import Dict

import pytest
import pytest_asyncio
import yaml
from pytest_idem import runner

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-machine-image-" + str(uuid.uuid4()),
    "instance_resource_id": "projects/tango-gcp/zones/us-central1-a/instances/idem-test-instance-"
    + str(uuid.uuid4()),
    "timestamp": str(int(time.time())),
}

ABSENT_STATE = """
projects/tango-gcp/global/machineImages/{name}:
  gcp.compute.machine_image.absent:
  - resource_id: projects/tango-gcp/global/machineImages/{name}
  - name: {name}
"""

PRESENT_STATE = """
projects/tango-gcp/global/machineImages/{name}:
  gcp.compute.machine_image.present:
  - name: {name}
  - project: tango-gcp
  - source_instance: {instance_resource_id}
  - storage_locations:
    - us
"""


@pytest_asyncio.fixture(scope="function")
async def new_instance_resource_id(hub, idem_cli, tests_dir) -> Dict[str, Any]:
    path_to = f"{tests_dir}/sls/compute/default_instance_present.sls"
    instance_resource_type = "compute.instance"
    expected_state = {}
    instance_name = ""
    resource_id = ""
    with open(path_to) as template:
        data_template = template.read()
        present_ret = hub.tool.utils.call_present_from_sls(
            idem_cli, data_template.format(**PARAMETER)
        )
        assert present_ret["result"], present_ret["comment"]

        resource_id = present_ret["new_state"].get("resource_id")
        present_state_dict = yaml.safe_load(data_template.format(**PARAMETER))
        instance_name = list(present_state_dict.keys())[0]
        expected_state = {
            "resource_id": resource_id,
            "name": instance_name,
            **dict(
                ChainMap(
                    *present_state_dict[instance_name]["gcp.compute.instance.present"]
                )
            ),
        }

        # We need to add disks[0].source to the expected_state
        # because the disk for this VM was created with disks[0].initialize_params
        # which is a create-only property and the get method returns the newly created disk source property instead
        expected_state["disks"][0]["source"] = present_ret["new_state"]["disks"][0][
            "source"
        ]
        changes = hub.tool.gcp.utils.compare_states(
            expected_state, copy.copy(present_ret["new_state"]), instance_resource_type
        )
        assert not bool(changes), changes

        yield resource_id

    absent_ret = hub.tool.utils.call_absent(
        idem_cli, instance_resource_type, instance_name, resource_id
    )
    assert absent_ret["result"], absent_ret["comment"]

    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(absent_ret["old_state"]), instance_resource_type
    )

    assert not bool(changes), changes
    assert not absent_ret.get("new_state")


@pytest.mark.dependency(name="present_test")
def test_machine_image_present_test(hub, ctx, idem_cli, tests_dir):
    global PARAMETER
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(PRESENT_STATE.format(**PARAMETER))
        state_ret = idem_cli(
            "state",
            fh,
            "--test",
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = state_ret[
            f'gcp.compute.machine_image_|-projects/tango-gcp/global/machineImages/{PARAMETER["name"]}_|-{PARAMETER["name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]

        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type="gcp.compute.machine_image", name=PARAMETER["name"]
            )
        ] == ret["comment"]
        assert PARAMETER["name"] == new_resource.get("name")


@pytest.mark.asyncio
@pytest.mark.dependency(name="present", depends=["present_test"])
async def test_machine_image_present_real(
    hub, ctx, idem_cli, tests_dir, new_instance_resource_id
):
    global PARAMETER
    PARAMETER["instance_resource_id"] = new_instance_resource_id
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(PRESENT_STATE.format(**PARAMETER))
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = state_ret[
            f'gcp.compute.machine_image_|-projects/tango-gcp/global/machineImages/{PARAMETER["name"]}_|-{PARAMETER["name"]}_|-present'
        ]
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]

        PARAMETER["resource_id"] = new_resource.get("resource_id", "")
        # TODO: Here we assert
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type="gcp.compute.machine_image", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
        assert PARAMETER["name"] == new_resource.get("name")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_machine_image_describe(hub, ctx):
    describe_ret = await hub.states.gcp.compute.machine_image.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "gcp.compute.machine_image.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "gcp.compute.machine_image.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert PARAMETER["resource_id"] == described_resource_map["resource_id"]


@pytest.mark.dependency(name="absent", depends=["describe"])
def test_machine_image_absent(hub, idem_cli, tests_dir):
    global PARAMETER
    if PARAMETER["resource_id"]:
        with runner.named_tempfile(suffix=".sls") as fh:
            fh.write_text(ABSENT_STATE.format(**PARAMETER))
            output_data = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
            ret = output_data[
                f'gcp.compute.machine_image_|-projects/tango-gcp/global/machineImages/{PARAMETER["name"]}_|-{PARAMETER["name"]}_|-absent'
            ]
            assert ret["result"], ret["comment"]
            # TODO we do not have old state returned (not ret.get("old_state")) is not ok to be None
            assert (not ret.get("old_state")) and (not ret.get("new_state"))
