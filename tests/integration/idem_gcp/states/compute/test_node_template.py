from collections import ChainMap

import pytest


PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe")
async def test_node_templates_describe(hub, ctx):
    global PARAMETER

    ret = await hub.states.gcp.compute.node_template.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.node_template.present")
        assert described_resource
        node_template = dict(ChainMap(*described_resource))
        assert node_template.get("resource_id") == resource_id
        if not PARAMETER.get("node_template_name"):
            PARAMETER["node_template_name"] = node_template.get("name")
            PARAMETER["node_template_region"] = node_template.get("region")


@pytest.mark.asyncio
@pytest.mark.dependency(name="list_filter", depends=["describe"])
async def test_node_templates_list_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.node_template.list(
        ctx,
        filter=f"name eq {PARAMETER['node_template_name']}",
    )
    assert len(ret["ret"]) == 1
    node_template = ret["ret"][0]
    assert node_template["name"] == f"{PARAMETER['node_template_name']}"

    ret = await hub.exec.gcp.compute.node_template.list(
        ctx,
        filter=f"name ne {PARAMETER['node_template_name']}",
    )
    for node_template in ret["ret"]:
        assert node_template["name"] != f"{PARAMETER['node_template_name']}"


@pytest.mark.asyncio
@pytest.mark.dependency(name="get", depends=["describe"])
async def test_node_template_get(hub, ctx):
    ret = await hub.exec.gcp.compute.node_template.get(
        ctx,
        region=f"{PARAMETER['node_template_region']}",
        node_template=f"{PARAMETER['node_template_name']}",
    )
    node_template = ret["ret"]
    assert node_template["name"] == f"{PARAMETER['node_template_name']}"

    ret = await hub.exec.gcp.compute.node_template.get(
        ctx,
        region=f"{PARAMETER['node_template_region']}",
        node_template="@@##$$%%^^",
    )
    assert not ret["ret"]
