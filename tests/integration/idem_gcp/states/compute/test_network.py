import uuid
from collections import ChainMap

import pytest
from pytest_idem import runner

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

PARAMETER = {
    "network_name": "idem-test-network-" + str(uuid.uuid4()),
    "network2_name": "idem-test-network-" + str(uuid.uuid4()),
    "peering_name": "idem-test-peering-" + str(uuid.uuid4()),
    "routing_mode": "REGIONAL",
}

NETWORK_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - description: ''
  - auto_create_subnetworks: true
  - peerings: []
  - routing_config:
      routing_mode: {routing_mode}
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
"""

NETWORK_WITH_PEERING_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - description: ''
  - auto_create_subnetworks: true
  - routing_config:
      routing_mode: REGIONAL
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
  - peerings:
    - auto_create_routes: true
      exchange_subnet_routes: true
      export_custom_routes: false
      export_subnet_routes_with_public_ip: true
      import_custom_routes: false
      import_subnet_routes_with_public_ip: false
      name: {peering_name}
      network: {peering_network}
      peer_mtu: 1520
"""

ABSENT_SPEC = """
test_network_absent_1:
  gcp.compute.network.absent:
  - resource_id: {resource_id}
"""


@pytest.fixture(scope="function")
def test_networks(idem_cli):
    global PARAMETER
    if "test_network" in PARAMETER and "test_network2" in PARAMETER:
        return [PARAMETER["test_network"], PARAMETER["test_network2"]]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": PARAMETER["network_name"],
                    "routing_mode": PARAMETER["routing_mode"],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )
        assert ret["result"], ret["comment"]
        network = ret["new_state"]
        PARAMETER["test_network"] = network

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_WITH_PEERING_SPEC.format(
                **{
                    "network_name": PARAMETER["network2_name"],
                    "peering_name": PARAMETER["peering_name"],
                    "peering_network": network.get("resource_id"),
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )
        assert ret["result"], ret["comment"]
        network = ret["new_state"]
        PARAMETER["test_network2"] = network

    return [PARAMETER["test_network"], PARAMETER["test_network2"]]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_patch_network")
def test_present_patch_network(ctx, hub, idem_cli, test_networks, __test):
    current_network = next(
        network
        for network in test_networks
        if network["name"] == PARAMETER["network_name"]
    )

    assert current_network.get("routing_config")
    assert current_network["routing_config"].get("routing_mode") == "REGIONAL"
    PARAMETER["routing_mode"] = "GLOBAL"

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": current_network["name"],
                    "routing_mode": PARAMETER["routing_mode"],
                }
            )
        )

        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json

        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )
        assert ret["result"], ret["comment"]

        if __test:
            assert [
                hub.tool.gcp.comment_utils.would_update_comment(
                    resource_type="gcp.compute.network", name=PARAMETER["network_name"]
                )
            ] == ret["comment"]
        else:
            assert (
                hub.tool.gcp.comment_utils.update_comment(
                    resource_type="gcp.compute.network", name=PARAMETER["network_name"]
                )
                in ret["comment"]
            )

            network = ret["new_state"]
            assert network.get("routing_config"), current_network.get("routing_config")
            assert network["routing_config"].get("routing_mode") != current_network[
                "routing_config"
            ].get("routing_mode")


@pytest.mark.dependency(
    name="present_remove_add_peering", depends=["present_patch_network"]
)
def test_present_remove_add_peering(hub, idem_cli, test_networks):
    assert len(test_networks) == 2

    assert test_networks[0].get("name") == PARAMETER["network_name"]
    assert not test_networks[0].get("peerings")

    assert test_networks[1].get("name") == PARAMETER["network2_name"]
    assert len(test_networks[1].get("peerings")) == 1

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": PARAMETER["network2_name"],
                    "routing_mode": PARAMETER["routing_mode"],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )
        assert ret["result"], ret["comment"]
        network = ret["new_state"]

        assert network.get("name") == PARAMETER["network2_name"]
        assert len(network.get("peerings", [])) == 0

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_WITH_PEERING_SPEC.format(
                **{
                    "network_name": PARAMETER["network2_name"],
                    "peering_name": PARAMETER["peering_name"],
                    "peering_network": PARAMETER["test_network"].get("resource_id"),
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )
        assert ret["result"], ret["comment"]

        network = ret["new_state"]
        assert network.get("name") == PARAMETER["network2_name"]
        assert len(network.get("peerings", [])) == 1


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present_remove_add_peering"])
async def test_network_describe(hub, ctx):
    ret = await hub.states.gcp.compute.network.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.network.present")
        assert described_resource
        network = dict(ChainMap(*described_resource))
        assert network.get("resource_id") == resource_id


@pytest.mark.asyncio
async def test_cleanup(hub, ctx, idem_cli, test_networks):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            ABSENT_SPEC.format(
                **{"resource_id": PARAMETER["test_network2"].get("resource_id")}
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )
        assert ret["result"], ret["comment"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            ABSENT_SPEC.format(
                **{"resource_id": PARAMETER["test_network"].get("resource_id")}
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )
        assert ret["result"], ret["comment"]

    ret = await hub.exec.gcp.compute.network.get(
        ctx, resource_id=PARAMETER["test_network"].get("resource_id")
    )
    assert ret["result"]
    assert not ret["ret"]

    ret = await hub.exec.gcp.compute.network.get(
        ctx, resource_id=PARAMETER["test_network2"].get("resource_id")
    )
    assert ret["result"]
    assert not ret["ret"]
