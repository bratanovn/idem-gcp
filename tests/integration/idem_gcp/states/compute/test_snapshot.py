import uuid
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

PARAMETER = {
    "snapshot_name": "idem-test-snapshot-" + str(uuid.uuid4()),
    "disk_name": "idem-test-disk-" + str(uuid.uuid4()),
}

SNAPSHOT_SPEC = """
{name}:
  gcp.compute.snapshot.present:
  - name: {name}
  - source_disk: {disk}
  - storage_locations:
    - us-central1"""

LABELS_SPEC = """
  - labels:
      label1: value1
      label2: value2
  - resource_id: {resource_id}
"""


@pytest.fixture(scope="function")
def test_disk(hub, idem_cli, tests_dir, __test):
    global PARAMETER

    if "test_disk" in PARAMETER:
        return PARAMETER["test_disk"]

    path_to_sls = tests_dir / "sls" / "compute" / "default_disk_present.sls"
    with open(path_to_sls) as f:
        sls_str = f.read()
        sls_str = sls_str.format(
            **{
                "name": PARAMETER["disk_name"],
                "size_gb": "1",
                "resource_policies": [],
                "labels": {},
                "label_fingerprint": "",
            }
        )
        ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str, __test)
        assert ret["result"], ret["comment"]

        disk = ret["new_state"]
        assert disk

        if not __test:
            PARAMETER["test_disk"] = disk

        return disk


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_snapshot_create(hub, ctx, idem_cli, test_disk, __test):
    global PARAMETER

    sls_str = SNAPSHOT_SPEC.format(
        **{"name": PARAMETER["snapshot_name"], "disk": test_disk.get("resource_id")}
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str, __test)
    assert ret["result"], ret["comment"]
    assert ret["new_state"]

    snapshot = ret["new_state"]
    PARAMETER["test_snapshot"] = snapshot

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type="gcp.compute.snapshot", name=PARAMETER["snapshot_name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type="gcp.compute.snapshot", name=PARAMETER["snapshot_name"]
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_snapshot_labels(hub, ctx, idem_cli, test_disk, __test):
    global PARAMETER

    snapshot = PARAMETER.get("test_snapshot")
    assert snapshot
    ret = await hub.exec.gcp.compute.snapshot.get(
        ctx, resource_id=snapshot.get("resource_id")
    )
    assert ret["result"]
    assert ret["ret"]
    assert not ret["ret"].get("labels")

    sls_str = SNAPSHOT_SPEC.format(
        **{"name": PARAMETER["snapshot_name"], "disk": test_disk.get("resource_id")}
    ) + LABELS_SPEC.format(**{"resource_id": snapshot.get("resource_id")})
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str, __test)
    assert ret["result"], ret["comment"]
    assert ret["new_state"]

    snapshot = ret["new_state"]
    PARAMETER["test_snapshot"] = snapshot

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type="gcp.compute.snapshot", name=PARAMETER["snapshot_name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type="gcp.compute.snapshot", name=PARAMETER["snapshot_name"]
            )
            in ret["comment"]
        )

    labels = snapshot.get("labels")
    assert labels
    assert len(labels) == 2
    assert labels.get("label1") == "value1"
    assert labels.get("label2") == "value2"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_snapshot_delete(hub, ctx, idem_cli, __test):
    global PARAMETER

    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.snapshot",
        PARAMETER["snapshot_name"],
        PARAMETER["test_snapshot"].get("resource_id"),
        test=__test,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state")
    assert not ret.get("new_state")

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type="gcp.compute.snapshot", name=PARAMETER["snapshot_name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type="gcp.compute.snapshot", name=PARAMETER["snapshot_name"]
            )
            in ret["comment"]
        )
        snapshot = PARAMETER.pop("test_snapshot")
        ret = await hub.exec.gcp.compute.snapshot.get(
            ctx, resource_id=snapshot.get("resource_id")
        )
        assert ret["result"]
        assert not ret["ret"]


@pytest.mark.asyncio
async def test_snapshot_describe(hub, ctx):
    ret = await hub.states.gcp.compute.snapshot.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.snapshot.present")
        assert described_resource
        snapshot = dict(ChainMap(*described_resource))
        assert snapshot.get("resource_id") == resource_id
        assert hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
            resource_id, "compute.snapshot"
        )
