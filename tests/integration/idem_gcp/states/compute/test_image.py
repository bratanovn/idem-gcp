import uuid
from collections import ChainMap

import pytest


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-image-" + str(uuid.uuid4()),
}


@pytest.mark.asyncio
async def test_present_no_op(hub, ctx):
    ret = await hub.states.gcp.compute.image.present(ctx, name=PARAMETER["name"])
    assert ret
    assert ret["result"]
    assert ret["comment"]
    assert ret["comment"] == [
        "No-op: There is no create/update function for gcp.compute.image"
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_absent_invalid_resource_id(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.gcp.compute.image.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id="/projects/invalid/global/images/invalid-name",
    )

    if __test:
        assert ret
        assert ret.get("comment")
        assert ret.get("comment") == [
            hub.tool.gcp.comment_utils.would_delete_comment(
                "gcp.compute.image", PARAMETER["name"]
            )
        ]
    else:
        # Considers the resource deleted.
        assert ret
        assert ret.get("comment")
        assert ret.get("comment") == [
            hub.tool.gcp.comment_utils.delete_comment(
                "gcp.compute.image", PARAMETER["name"]
            )
        ]


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.image.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.image.present")
        assert described_resource
        image = dict(ChainMap(*described_resource))
        assert image.get("resource_id") == resource_id
