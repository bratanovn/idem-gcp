import copy
import uuid
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "network_name": "idem-test-network-" + str(uuid.uuid4()),
    "subnetwork_name": "idem-test-subnetwork-" + str(uuid.uuid4()),
}

NETWORK_PRESENT_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - description: ''
  - auto_create_subnetworks: false
  - peerings: []
  - routing_config:
      routing_mode: REGIONAL
  - mtu: 1460
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
"""

SUBNETWORK_PRESENT_SPEC = """
{subnetwork_name}:
  gcp.compute.subnetwork.present:
  - name: {subnetwork_name}
  - description: ''
  - network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/{network_name}
  - ip_cidr_range: {cidr_range}
  - region: us-central1
  - stack_type: {stack_type}
"""


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.asyncio
@pytest.mark.dependency(name="test_present_create")
async def test_present_create(hub, ctx, idem_cli, __test):
    global PARAMETER

    sls_str = NETWORK_PRESENT_SPEC.format(
        **{
            "network_name": PARAMETER["network_name"],
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str, __test)
    assert ret["result"], ret["comment"]
    PARAMETER["test_network"] = ret["new_state"]

    sls_str = SUBNETWORK_PRESENT_SPEC.format(
        **{
            "network_name": PARAMETER["network_name"],
            "subnetwork_name": PARAMETER["subnetwork_name"],
            "cidr_range": "10.200.0.0/20",
            "stack_type": "IPV4_ONLY",
        }
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str, __test)
    assert ret["result"], ret["comment"]

    subnetwork = ret["new_state"]
    assert subnetwork
    assert not ret["old_state"]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        subnetwork.get("resource_id"), "compute.subnetwork"
    )
    assert subnetwork.get("name") == PARAMETER["subnetwork_name"]
    PARAMETER["test_subnetwork"] = subnetwork

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type="gcp.compute.subnetwork",
                name=PARAMETER["subnetwork_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type="gcp.compute.subnetwork",
                name=PARAMETER["subnetwork_name"],
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.asyncio
@pytest.mark.dependency(name="test_present_update", depends=["test_present_create"])
async def test_present_update(hub, ctx, idem_cli, __test):
    global PARAMETER

    old_subnetwork = copy.copy(PARAMETER["test_subnetwork"])
    assert old_subnetwork["stack_type"] == "IPV4_ONLY"
    old_subnetwork["stack_type"] = "IPV4_IPV6"
    old_subnetwork["ipv6_access_type"] = "EXTERNAL"
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, "compute.subnetwork", old_subnetwork, __test
    )
    assert ret["result"], ret["comment"]
    updated_subnetwork = ret["new_state"]
    assert updated_subnetwork["stack_type"] == "IPV4_IPV6"

    # Update cidr range
    assert updated_subnetwork["ip_cidr_range"] == "10.200.0.0/20"
    updated_subnetwork["ip_cidr_range"] = "10.200.0.0/19"
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, "compute.subnetwork", updated_subnetwork, __test
    )
    assert ret["result"], ret["comment"]
    updated_subnetwork = ret["new_state"]
    assert updated_subnetwork["ip_cidr_range"] == "10.200.0.0/19"


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.asyncio
@pytest.mark.dependency(name="test_absent", depends=["test_present_update"])
async def test_absent(hub, ctx, idem_cli, __test):
    global PARAMETER

    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.subnetwork",
        PARAMETER["subnetwork_name"],
        PARAMETER["test_subnetwork"].get("resource_id"),
        test=__test,
    )
    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type="gcp.compute.subnetwork",
                name=PARAMETER["subnetwork_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type="gcp.compute.subnetwork",
                name=PARAMETER["subnetwork_name"],
            )
            in ret["comment"]
        )

    ret = await hub.exec.gcp.compute.subnetwork.get(
        ctx, resource_id=PARAMETER["test_subnetwork"].get("resource_id")
    )

    assert ret["result"]

    if __test:
        assert ret["ret"]
    else:
        assert not ret["ret"]

    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.network",
        PARAMETER["network_name"],
        PARAMETER["test_network"].get("resource_id"),
        test=__test,
    )
    assert ret["result"], ret["comment"]


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.subnetwork.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.subnetwork.present")
        assert described_resource
        subnetwork = dict(ChainMap(*described_resource))
        assert subnetwork.get("resource_id") == resource_id
