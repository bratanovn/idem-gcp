import copy
import uuid
from collections import ChainMap

import pytest

from tests.utils import create_external_instance
from tests.utils import delete_instance
from tests.utils import stop_instance
from tests.utils import wait_for_operation

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-instance-" + str(uuid.uuid4()),
    "disk_name": "idem-test-disk-" + str(uuid.uuid4()),
    "zone": "us-central1-a",
    "project": "tango-gcp",
    "external_instance_name": "idem-test-ext-instance-" + str(uuid.uuid4()),
    "instance_get_resource_only_with_resource_id": "idem-test-absent-instance-"
    + str(uuid.uuid4()),
}

PRESENT_STATE_DISK = {
    "name": PARAMETER["disk_name"],
    "zone": PARAMETER["zone"],
    "project": PARAMETER["project"],
    "type": "https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced",
    "size_gb": "10",
    "physical_block_size_bytes": "4096",
    "resource_policies": [
        "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1"
    ],
    "guest_os_features": [{"type": "UEFI_COMPATIBLE"}],
}

PRESENT_CREATE_STATE = {
    "name": PARAMETER["name"],
    "project": PARAMETER["project"],
    "zone": PARAMETER["zone"],
    "machine_type": "https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small",
    "can_ip_forward": False,
    "network_interfaces": [
        {
            "kind": "compute#networkInterface",
            "name": "nic0",
            "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
            "stack_type": "IPV4_ONLY",
            "subnetwork": "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default",
        }
    ],
    "disks": [
        {
            "auto_delete": True,
            "boot": True,
            "device_name": PARAMETER["disk_name"],
            "source": f"https://www.googleapis.com/compute/v1/projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/disks/{PARAMETER['disk_name']}",
            "mode": "READ_WRITE",
            "type": "PERSISTENT",
            "disk_size_gb": "10",
            "index": 0,
            "interface": "SCSI",
            "kind": "compute#attachedDisk",
            "guest_os_features": [{"type": "UEFI_COMPATIBLE"}],
        }
    ],
    "scheduling": {
        "automatic_restart": True,
        "on_host_maintenance": "MIGRATE",
        "preemptible": False,
        "provisioning_model": "STANDARD",
    },
    "deletion_protection": False,
    "tags": {"items": ["test"]},
    "metadata": {
        "kind": "compute#metadata",
        "items": [{"key": "sample_metadata_key", "value": "sample_metadata_value"}],
    },
    "shielded_instance_config": {
        "enable_secure_boot": False,
        "enable_vtpm": True,
        "enable_integrity_monitoring": True,
    },
    "shielded_instance_integrity_policy": {"update_auto_learn_policy": True},
    "status": "RUNNING",
}

PRESENT_UPDATED_DESCRIPTION = {
    "description": "Only description is updated.",
}

PRESENT_UPDATED_PROPERTIES = {
    "description": "test description",
    "tags": {"items": ["test1", "test2", "test3"]},
    "can_ip_forward": True,
    "network_interfaces": [
        {
            "access_configs": [
                {
                    "kind": "compute#accessConfig",
                    "name": "External NAT",
                    "network_tier": "PREMIUM",
                    "set_public_ptr": False,
                    "type": "ONE_TO_ONE_NAT",
                }
            ],
            "kind": "compute#networkInterface",
            "name": "nic0",
            "network": "https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default",
            "stack_type": "IPV4_ONLY",
            "subnetwork": "https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default",
        }
    ],
}

PRESENT_UPDATED_SHIELDED_CONFIG = {
    "shielded_instance_config": {
        "enable_secure_boot": True,
        "enable_vtpm": False,
        "enable_integrity_monitoring": False,
    }
}

PRESENT_UPDATED_NON_UPDATABLE_PROPERTIES = {
    "disks": [
        {
            "initialize_params": {
                "disk_name": "test_disk_name",
                "source_image": "projects/debian-cloud/global/images/family/debian-11",
                "disk_size_gb": "10",
            }
        }
    ],
}

NON_UPDATABLE_PROPERTIES_PATHS = {"disks[].initialize_params"}

RESOURCE_ID = f"projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/instances/{PARAMETER['name']}"

PRESENT_WITH_RESOURCE_ID = {
    "resource_id": RESOURCE_ID,
}

RESOURCE_TYPE_INSTANCES = "compute.instance"
RESOURCE_TYPE_DISKS = "compute.disk"

EXTERNAL_INSTANCE_SPEC = f"""
{PARAMETER["external_instance_name"]}:
  gcp.compute.instance.present:
  - name: {PARAMETER["external_instance_name"]}
  - project: tango-gcp
  - machine_type: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small
  - zone: us-central1-a
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        type: ONE_TO_ONE_NAT
      kind: compute#networkInterface
      network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default
  - disks:
    - auto_delete: true
      boot: true
"""


@pytest.fixture(scope="function")
def new_disk(hub, idem_cli):
    disk_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_DISKS, PRESENT_STATE_DISK
    )
    assert disk_present_ret["result"], disk_present_ret["comment"]

    resource_id = disk_present_ret["new_state"]["resource_id"]
    expected_state = {"resource_id": resource_id, **PRESENT_STATE_DISK}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(disk_present_ret["new_state"]), RESOURCE_TYPE_DISKS
    )
    assert not bool(changes), changes

    yield disk_present_ret

    disk_absent_ret = hub.tool.utils.call_absent(
        idem_cli, "compute.disk", disk_present_ret["new_state"]["name"], resource_id
    )
    assert disk_absent_ret["result"], disk_absent_ret["comment"]
    assert not disk_absent_ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_instance_present_create(hub, idem_cli, tests_dir, __test, new_disk):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, PRESENT_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    expected_state = {"resource_id": RESOURCE_ID, **PRESENT_CREATE_STATE}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )
    assert not bool(changes), changes


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id", depends=["present_create"]
)
def test_instance_present_get_resource_only_with_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    present_state.update(PRESENT_WITH_RESOURCE_ID)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )
    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.already_exists_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
        )
    ] == ret["comment"]

    expected_state = {"resource_id": RESOURCE_ID, **present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_instance_present_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert not ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                "gcp.compute.instance", name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="dont_update_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_instance_present_dont_update_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    present_state.update(PRESENT_UPDATED_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert not ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                "gcp.compute.instance", name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="update_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_instance_present_update_get_resource_only_with_resource_id_with_resource_id(
    hub, idem_cli, tests_dir, __test
):
    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_DESCRIPTION)
    update_present_state.update(PRESENT_WITH_RESOURCE_ID)

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        update_present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                "gcp.compute.instance", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )
    assert not bool(changes), changes


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_update",
    depends=["update_get_resource_only_with_resource_id_missing_resource_id"],
)
def test_instance_present_update(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )
    assert not bool(changes), changes


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_already_exists", depends=["present_update"])
def test_instance_present_already_exists(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.already_exists_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
        )
    ] == ret["comment"]

    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_changed_non_updatable_properties", depends=["present_already_exists"]
)
def test_instance_present_changed_non_updatable_properties(
    hub, idem_cli, tests_dir, __test
):
    global PARAMETER
    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_NON_UPDATABLE_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, update_present_state, __test
    )

    assert not ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            f"gcp.{RESOURCE_TYPE_INSTANCES}",
            PARAMETER["name"],
            NON_UPDATABLE_PROPERTIES_PATHS,
        )
    ] == ret["comment"]

    # This is the current old_state and new_state after the failed update above
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    present_state.update(PRESENT_UPDATED_PROPERTIES)

    expected_state = {"resource_id": RESOURCE_ID, **present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="instance_present_update_shielded_config_after_power_off",
    depends=["present_create"],
)
def test_instance_present_update_shielded_config_after_power_off(
    hub, ctx, idem_cli, tests_dir, __test
):
    global PARAMETER

    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    update_present_state.update(PRESENT_UPDATED_SHIELDED_CONFIG)

    # PRESENT_UPDATED_SHIELDED_CONFIG is separated from PRESENT_UPDATED_PROPERTIES because it needs power off
    # consequent tests need to take the update nto account
    PRESENT_UPDATED_PROPERTIES.update(PRESENT_UPDATED_SHIELDED_CONFIG)
    PRESENT_UPDATED_PROPERTIES.update(**{"status": "TERMINATED"})

    if not __test:
        project = PARAMETER["project"]
        zone = PARAMETER["zone"]
        name = PARAMETER["name"]
        op = stop_instance(ctx, project, zone, name)
        wait_for_operation(ctx, project, zone, op)

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCES, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["new_state"]), RESOURCE_TYPE_INSTANCES
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="describe", depends=["present_changed_non_updatable_properties"]
)
async def test_instance_describe(hub, ctx):
    describe_ret = await hub.states.gcp.compute.instance.describe(ctx)
    assert RESOURCE_ID in describe_ret
    assert f"gcp.{RESOURCE_TYPE_INSTANCES}.present" in describe_ret[RESOURCE_ID]
    described_resource = describe_ret[RESOURCE_ID].get(
        f"gcp.{RESOURCE_TYPE_INSTANCES}.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert RESOURCE_ID == described_resource_map["resource_id"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="absent_get_resource_only_with_resource_id_missing_resource_id",
    depends=["describe"],
)
def test_instance_absent_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        PARAMETER["name"],
        None,
        test=__test,
        additional_kwargs=["--get-resource-only-with-resource-id"],
    )

    assert [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="absent",
    depends=["absent_get_resource_only_with_resource_id_missing_resource_id"],
)
def test_instance_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        PARAMETER["name"],
        RESOURCE_ID,
        test=__test,
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    update_present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    expected_state = {"resource_id": RESOURCE_ID, **update_present_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, copy.copy(ret["old_state"]), RESOURCE_TYPE_INSTANCES
    )

    assert not bool(changes), changes
    assert not ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
def test_instance_already_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        PARAMETER["name"],
        RESOURCE_ID,
        test=__test,
    )

    assert [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=PARAMETER["name"]
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.dependency(name="test_instance_dont_absent_only_by_name")
@pytest.mark.asyncio
def test_instance_dont_absent_only_by_name(hub, ctx, idem_cli, tests_dir):
    global PARAMETER
    project = "tango-gcp"
    zone = "us-central1-a"
    name = PARAMETER["instance_get_resource_only_with_resource_id"]

    # creates a GCP instance using the GCP API client library without going through idem
    create_external_instance(ctx, project, zone, name)

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        name,
        resource_id=None,
        test=False,
    )

    assert [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=name
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.dependency(
    name="test_instance_absent_by_name_zone_project",
    depends=["test_instance_dont_absent_only_by_name"],
)
@pytest.mark.asyncio
def test_instance_absent_by_name_zone_project(hub, ctx, idem_cli, tests_dir):
    global PARAMETER
    project = "tango-gcp"
    zone = "us-central1-a"
    name = PARAMETER["instance_get_resource_only_with_resource_id"]

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCES,
        name,
        None,
        test=False,
        zone=zone,
        project=project,
    )

    assert [
        hub.tool.gcp.comment_utils.delete_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCES}", name=name
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert ret.get("old_state")


@pytest.mark.asyncio
async def test_discover_external_instance(hub, ctx, idem_cli, tests_dir):
    project = "tango-gcp"
    zone = "us-central1-a"
    name = PARAMETER["external_instance_name"]

    # creates a GCP instance using the GCP API client library without going through idem
    create_external_instance(ctx, project, zone, name)

    ret = hub.tool.utils.call_present_from_sls(idem_cli, EXTERNAL_INSTANCE_SPEC)

    assert ret["result"]
    assert (
        hub.tool.gcp.comment_utils.already_exists_comment("gcp.compute.instance", name)
        in ret["comment"]
    )
    assert ret["new_state"] == ret["old_state"]
    assert (
        ret["new_state"]["resource_id"]
        == f"projects/{project}/zones/{zone}/instances/{name}"
    )

    # deletes the created instance using the GCP API client library
    delete_instance(ctx, project, zone, name)
