import uuid
from collections import ChainMap

import pytest
from pytest_idem import runner

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-disk-" + str(uuid.uuid4()),
    "size_gb": "1",
    "resource_policies": [],
    "labels": {},
    "label_fingerprint": "",
}

ABSENT_STATE = f"""
{PARAMETER["name"]}:
  gcp.compute.disk.absent:
  - resource_id: projects/tango-gcp/zones/us-central1-a/disks/{PARAMETER["name"]}
  - project: tango-gcp
  - zone: us-central1-a
"""


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_disk_present(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    path_to = f"{tests_dir}/sls/compute/default_disk_present.sls"
    with open(path_to) as template:
        data_template = template.read()
        with runner.named_tempfile(suffix=".sls") as fh:
            fh.write_text(data_template.format(**PARAMETER))
            if __test:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--test",
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            else:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            ret = state_ret[
                f'gcp.compute.disk_|-{PARAMETER["name"]}_|-{PARAMETER["name"]}_|-present'
            ]
            assert ret["result"], ret["comment"]
            new_resource = ret["new_state"]
        if __test:
            assert [
                hub.tool.gcp.comment_utils.would_create_comment(
                    resource_type="gcp.compute.disk", name=PARAMETER["name"]
                )
            ] == ret["comment"]
        else:
            PARAMETER["resource_id"] = new_resource.get("resource_id", "")
            PARAMETER["label_fingerprint"] = new_resource.get("label_fingerprint", "")
            # TODO: Here we assert
            assert (
                hub.tool.gcp.comment_utils.create_comment(
                    resource_type="gcp.compute.disk", name=PARAMETER["name"]
                )
                in ret["comment"]
            )
        assert PARAMETER["name"] == new_resource.get("name")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["present"])
def test_disk_update(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    PARAMETER["resource_policies"] = [
        "projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1"
    ]
    PARAMETER["labels"] = {"label_key_test": "label_value_test"}
    PARAMETER["size_gb"] = "2"
    path_to = f"{tests_dir}/sls/compute/default_disk_present.sls"
    with open(path_to) as template:
        data_template = template.read()
        with runner.named_tempfile(suffix=".sls") as fh:
            fh.write_text(data_template.format(**PARAMETER))
            if __test:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--test",
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            else:
                state_ret = idem_cli(
                    "state",
                    fh,
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
            ret = state_ret[
                f'gcp.compute.disk_|-{PARAMETER["name"]}_|-{PARAMETER["name"]}_|-present'
            ]
            assert ret["result"], ret["comment"]
            updated_resource = ret["new_state"]

        if __test:
            assert [
                hub.tool.gcp.comment_utils.would_update_comment(
                    resource_type="gcp.compute.disk", name=PARAMETER["name"]
                )
            ] == ret["comment"]
        else:
            # TODO: Here we assert
            assert (
                hub.tool.gcp.comment_utils.update_comment(
                    resource_type="gcp.compute.disk", name=PARAMETER["name"]
                )
                in ret["comment"]
            )

            updated_resource["resource_policies"] = [
                hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
                    policy, "compute.resource_policy"
                )
                for policy in updated_resource.get("resource_policies")
            ]

            assert PARAMETER["labels"] == updated_resource.get("labels")
            assert PARAMETER["size_gb"] == updated_resource.get("size_gb")
            assert hub.tool.gcp.state_comparison_utils.are_lists_identical(
                PARAMETER["resource_policies"], updated_resource["resource_policies"]
            )

        assert PARAMETER["name"] == updated_resource.get("name")
        assert PARAMETER["resource_id"] == updated_resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["update"])
async def test_disk_describe(hub, ctx):
    describe_ret = await hub.states.gcp.compute.disk.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "gcp.compute.disk.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("gcp.compute.disk.present")
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert PARAMETER["resource_id"] == described_resource_map["resource_id"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
def test_disk_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.disk",
        PARAMETER["name"],
        PARAMETER["resource_id"],
        test=__test,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state")
    assert not ret.get("new_state")

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type="gcp.compute.disk", name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type="gcp.compute.disk", name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
def test_instance_already_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.disk",
        PARAMETER["name"],
        PARAMETER["resource_id"],
        test=__test,
    )

    assert [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.compute.disk", name=PARAMETER["name"]
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")
