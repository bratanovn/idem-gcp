import uuid
from collections import ChainMap

import pytest


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-resource_policy-" + str(uuid.uuid4()),
}


@pytest.mark.asyncio
async def test_present_no_op(hub, ctx):
    ret = await hub.states.gcp.compute.resource_policy.present(
        ctx, name=PARAMETER["name"]
    )
    assert ret
    assert ret["result"]
    assert ret["comment"]
    assert ret["comment"] == [
        "No-op: There is no create/update function for gcp.compute.resource_policy"
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_absent_invalid_resource_id(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.gcp.compute.resource_policy.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id="/projects/invalid/region/invalid/resourcePolicies/invalid-name",
    )

    if __test:
        assert ret
        assert ret.get("comment")
        assert ret.get("comment") == [
            hub.tool.gcp.comment_utils.would_delete_comment(
                "gcp.compute.resource_policy", PARAMETER["name"]
            )
        ]
    else:
        # Considers the resource deleted.
        assert ret
        assert ret.get("comment")
        assert ret.get("comment") == [
            hub.tool.gcp.comment_utils.delete_comment(
                "gcp.compute.resource_policy", PARAMETER["name"]
            )
        ]


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.resource_policy.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.resource_policy.present")
        assert described_resource
        resource_policy = dict(ChainMap(*described_resource))
        assert resource_policy.get("resource_id") == resource_id
