import time

import pytest
from pytest_idem import runner

idem_name = ("idem-test-location-" + str(int(time.time())),)
project_id = "tango-gcp"
location_id = "us-east1"

ABSENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.location.absent:
  - resource_id: projects/{project_id}/locations/{location_id}
"""

PRESENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.location.present:
  - resource_id: projects/{project_id}/locations/{location_id}
"""

PARAMETRIZE = dict(
    argnames="__test, __state",
    argvalues=[
        (v, k)
        for v in [True, False]
        for k in [("absent", ABSENT_STATE), ("present", PRESENT_STATE)]
    ],
    ids=[f"{v}-{k}" for v in ["--test", "run"] for k in ["absent", "present"]],
)


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_absent")
def test_location_present_absent(hub, idem_cli, tests_dir, __test, __state):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(__state[1])
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = state_ret[
            f"gcp.cloudkms.location_|-{idem_name}_|-{idem_name}_|-{__state[0]}"
        ]
        assert not ret["result"], ret["comment"]
        assert ret["comment"][
            0
        ] == hub.tool.gcp.comment_utils.no_resource_create_update_comment(
            "gcp.cloudkms.location"
        )


@pytest.mark.asyncio
async def test_location_describe(hub, ctx):
    ret = await hub.states.gcp.cloudkms.location.describe(ctx)
    assert len(ret) > 0
    assert ret["projects/tango-gcp/locations/global"]
