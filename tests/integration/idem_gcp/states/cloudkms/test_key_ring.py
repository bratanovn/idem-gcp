import time

import pytest
from pytest_idem import runner

idem_name = ("idem-test-key_ring-" + str(int(time.time())),)
project_id = "tango-gcp"
location_id = "us-east1"
key_ring_id = "cicd-idem-gcp-1"

ABSENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.key_ring.absent:
  - resource_id: projects/{project_id}/locations/{location_id}/keyRings/{key_ring_id}
"""

PRESENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.key_ring.present:
    - project_id: {project_id}
    - location_id: {location_id}
    - key_ring_id:  {key_ring_id}

"""

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_key_ring_present(hub, idem_cli, tests_dir, __test):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(PRESENT_STATE)
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = state_ret[f"gcp.cloudkms.key_ring_|-{idem_name}_|-{idem_name}_|-present"]
        assert ret["result"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
def test_key_ring_absent(hub, idem_cli, tests_dir, __test):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(ABSENT_STATE)
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = state_ret[f"gcp.cloudkms.key_ring_|-{idem_name}_|-{idem_name}_|-absent"]
        assert not ret["result"], ret["comment"]


@pytest.mark.asyncio
async def test_key_ring_describe(hub, ctx):
    ret = await hub.states.gcp.cloudkms.key_ring.describe(ctx)
    assert len(ret) > 0
