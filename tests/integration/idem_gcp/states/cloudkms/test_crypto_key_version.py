import time

import pytest
from pytest_idem import runner

idem_name = ("idem-test-crypto_key_version-" + str(int(time.time())),)
project_id = "tango-gcp"
location_id = "us-east1"
key_ring_id = "cicd-idem-gcp-1"
crypto_key_id = "cicd-key-1"
crypto_key_version_id = "3"

ABSENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.crypto_key_version.absent:
  - resource_id: projects/{project_id}/locations/{location_id}/keyRings/{key_ring_id}/cryptoKeys/{crypto_key_id}/cryptoKeyVersions/{crypto_key_version_id}
"""

PRESENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.crypto_key_version.present:
    - key_state: ENABLED
    - project_id: {project_id}
    - location_id: {location_id}
    - key_ring_id:  {key_ring_id}
    - crypto_key_id: {crypto_key_id}
    - crypto_key_version_id: {crypto_key_version_id}
"""

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
def test_crypto_key_version_absent(hub, idem_cli, tests_dir, __test):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(ABSENT_STATE)
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = state_ret[
            f"gcp.cloudkms.crypto_key_version_|-{idem_name}_|-{idem_name}_|-absent"
        ]
        assert ret["result"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present", depends=["absent"])
def test_crypto_key_version_present(hub, idem_cli, tests_dir, __test):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(PRESENT_STATE)
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = state_ret[
            f"gcp.cloudkms.crypto_key_version_|-{idem_name}_|-{idem_name}_|-present"
        ]
        assert ret["result"], ret["comment"]


@pytest.mark.asyncio
async def test_crypto_key_version_describe(hub, ctx):
    ret = await hub.states.gcp.cloudkms.crypto_key_version.describe(ctx)
    assert len(ret) > 2
