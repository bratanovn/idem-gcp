import time

import pytest
from pytest_idem import runner

idem_name = ("idem-test-crypto_key-" + str(int(time.time())),)
project_id = "tango-gcp"
location_id = "us-east1"
key_ring_id = "cicd-idem-gcp-1"
crypto_key_id = "cicd-key-1"
crypto_key_version_id = "1" if int(time.time()) % 2 == 0 else "2"

ABSENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.crypto_key.absent:
  - resource_id: projects/{project_id}/locations/{location_id}/keyRings/{key_ring_id}/cryptoKeys/{crypto_key_id}
"""

PRESENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.crypto_key.present:
    - primary:
        name: projects/{project_id}/locations/{location_id}/keyRings/{key_ring_id}/cryptoKeys/{crypto_key_id}/cryptoKeyVersions/{crypto_key_version_id}
    - purpose: ENCRYPT_DECRYPT
    - labels:
        lbl_key_1: lbl-value-1
        lbl_key_2: lbl-value-2
    - version_template:
        algorithm: GOOGLE_SYMMETRIC_ENCRYPTION
        protection_level: SOFTWARE
    - destroy_scheduled_duration: 186400s
    - rotation_period: 31500001s
    - next_rotation_time: "2034-10-02T15:01:23Z"
    - project_id: {project_id}
    - location_id: {location_id}
    - key_ring_id:  {key_ring_id}
    - crypto_key_id: {crypto_key_id}

"""

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_crypto_key_present(hub, idem_cli, tests_dir, __test):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(PRESENT_STATE)
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = state_ret[
            f"gcp.cloudkms.crypto_key_|-{idem_name}_|-{idem_name}_|-present"
        ]
        assert ret["result"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
def test_crypto_key_absent(hub, idem_cli, tests_dir, __test):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(ABSENT_STATE)
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = state_ret[f"gcp.cloudkms.crypto_key_|-{idem_name}_|-{idem_name}_|-absent"]
        assert not ret["result"], ret["comment"]


@pytest.mark.asyncio
async def test_crypto_key_describe(hub, ctx):
    ret = await hub.states.gcp.cloudkms.crypto_key.describe(ctx)
    assert len(ret) > 0
