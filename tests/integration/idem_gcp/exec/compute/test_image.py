from typing import Any
from typing import Dict
from typing import List

import pytest


@pytest.fixture(scope="module")
async def gcp_images(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.image.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_project_filter(hub, ctx, gcp_images):
    if len(gcp_images) > 0:
        image = gcp_images[0]

        ret = await hub.exec.gcp.compute.image.list(
            ctx,
            filter=f"name eq {image['name']}",
        )

        assert ret["ret"], ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_project_name(hub, ctx, gcp_images):
    if len(gcp_images) > 0:
        image = gcp_images[0]

        ret = await hub.exec.gcp.compute.image.get(
            ctx,
            name=f"{image['name']}",
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_image = ret["ret"]
        assert image["name"] == returned_image["name"]
        assert image.get("resource_id") == returned_image.get("resource_id")
        assert image.get("name") in returned_image.get("name")


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_images):
    if len(gcp_images) > 0:
        image = gcp_images[0]

        ret = await hub.exec.gcp.compute.image.get(
            ctx,
            resource_id=image["resource_id"],
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_image = ret["ret"]
        assert image["name"] == returned_image["name"]
        assert image.get("resource_id") == returned_image.get("resource_id")
        assert image.get("name") in returned_image.get("name")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_images):
    if len(gcp_images) > 0:
        ret = await hub.exec.gcp.compute.image.get(
            ctx,
            name="invalid-name",
        )
        assert ret["result"], not ret["ret"]
        assert "Get compute.image 'None' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = "/projects/invalid/global/images/invalid-name"
    ret = await hub.exec.gcp.compute.image.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
