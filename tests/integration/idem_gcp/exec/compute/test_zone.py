from typing import Any
from typing import Dict
from typing import List

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module")
async def gcp_zones(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.zone.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_get_missing_name_arg(hub, ctx):
    ret = await hub.exec.gcp.compute.zone.get(ctx)
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
    assert (
        f"gcp.compute.zone None either resource_id or project and name "
        f"should be specified." in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, gcp_zones):
    zone = gcp_zones[0]
    ret = await hub.exec.gcp.compute.zone.get(
        ctx,
        name=zone.get("name"),
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert zone.get("resource_id") == ret["ret"].get("resource_id")
    assert zone.get("name") in ret["ret"].get("name")
    assert zone.get("id") in ret["ret"].get("id")


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, gcp_zones):
    zone = gcp_zones[0]
    ret = await hub.exec.gcp.compute.zone.get(ctx, resource_id=zone.get("resource_id"))
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert zone.get("resource_id") == ret["ret"].get("resource_id")
    assert zone.get("name") in ret["ret"].get("name")
    assert zone.get("id") in ret["ret"].get("id")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx):
    invalid_name = "invalid-zone-name"
    ret = await hub.exec.gcp.compute.zone.get(
        ctx,
        name=invalid_name,
    )
    assert ret["result"], not ret["ret"]
    assert (
        hub.tool.gcp.comment_utils.get_empty_comment(
            resource_type="compute.zone", name=None
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = f"/projects/{ctx.acct.project_id}/zones/invalid-zone"
    ret = await hub.exec.gcp.compute.zone.get(ctx, resource_id=invalid_resource_id)

    assert ret["result"], not ret["ret"]
    assert (
        hub.tool.gcp.comment_utils.get_empty_comment(
            resource_type="compute.zone", name=invalid_resource_id
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_list_invalid_project(hub, ctx):
    ret = await hub.exec.gcp.compute.zone.list(ctx, project="invalid-project")
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_list_for_project(hub, ctx):
    ret = await hub.exec.gcp.compute.zone.list(ctx)
    assert ret["result"], ret["ret"]
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_list_filter_id(hub, ctx, gcp_zones):
    zone_random = gcp_zones[0]
    ret = await hub.exec.gcp.compute.zone.list(
        ctx,
        filter=f"id={zone_random['id']}",
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) == 1
    list_ret_resource = ret["ret"][0]
    assert zone_random.get("zone") == list_ret_resource.get("zone")
    assert zone_random.get("id") == list_ret_resource.get("id")
    assert zone_random.get("name") == list_ret_resource.get("name")


@pytest.mark.asyncio
async def test_list_filter_name(hub, ctx, gcp_zones):
    zone_random = gcp_zones[0]
    ret = await hub.exec.gcp.compute.zone.list(
        ctx,
        filter=f"name={zone_random['name']}",
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) == 1
    list_ret_resource = ret["ret"][0]
    assert zone_random.get("zone") == list_ret_resource.get("zone")
    assert zone_random.get("id") == list_ret_resource.get("id")
    assert zone_random.get("name") == list_ret_resource.get("name")


@pytest.mark.asyncio
async def test_list_invalid_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.zone.list(ctx, filter=f"name eq unknown")
    assert ret["result"], not ret["ret"]
    assert not ret["comment"]
