import uuid
from typing import Any
from typing import Dict
from typing import List

import pytest


@pytest.fixture(scope="module")
async def gcp_disks(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.disk.list(ctx, zone="us-central1-a")
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_project_zone_filter(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disk.list(
            ctx,
            zone="us-central1-a",
            filter=f"name eq {disk['name']}",
        )

        assert ret["ret"]
        assert ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_project_zone_name(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disk.get(
            ctx,
            zone="us-central1-a",
            name=f"{disk['name']}",
        )

        assert ret["result"]
        assert ret["ret"]
        assert not ret["comment"]

        returned_disk = ret["ret"]
        assert disk["name"] == returned_disk["name"]
        assert disk.get("resource_id") == returned_disk.get("resource_id")
        assert disk.get("name") in returned_disk.get("name")
        assert disk.get("zone") in returned_disk.get("zone")


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disk.get(
            ctx,
            resource_id=disk["resource_id"],
        )

        assert ret["result"]
        assert ret["ret"]
        assert not ret["comment"]

        returned_disk = ret["ret"]
        assert disk["name"] == returned_disk["name"]
        assert disk.get("resource_id") == returned_disk.get("resource_id")
        assert disk.get("name") in returned_disk.get("name")
        assert disk.get("zone") in returned_disk.get("zone")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]
        ret = await hub.exec.gcp.compute.disk.get(
            ctx,
            zone=disk.get("zone"),
            name="invalid-name",
        )
        assert ret["result"]
        assert not ret["ret"]
        assert "Get compute.disk 'None' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = "/projects/invalid/zone/invalid/disks/invalid-name"
    ret = await hub.exec.gcp.compute.disk.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_create_snapshot(hub, ctx, gcp_disks):
    if not gcp_disks:
        return

    disk = gcp_disks[0]
    snapshot_name = "idem-test-snapshot-" + str(uuid.uuid4())
    ret = await hub.exec.gcp.compute.disk.create_snapshot(
        ctx,
        resource_id=disk.get("resource_id"),
        name=snapshot_name,
        storage_locations=["us-central1"],
    )
    assert ret["result"]
    assert ret["ret"], ret["comment"]
