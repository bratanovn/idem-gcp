from typing import Any
from typing import Dict
from typing import List

import pytest

_REGION = "us-central1"


@pytest.fixture(scope="module")
async def gcp_subnetworks(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.subnetwork.list(ctx, region=_REGION)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_project_region_filter_aip(hub, ctx, gcp_subnetworks):
    if len(gcp_subnetworks) > 0:
        subnetwork = gcp_subnetworks[0]

        ret = await hub.exec.gcp.compute.subnetwork.list(
            ctx,
            region=_REGION,
            filter=f"name = {subnetwork['name']}",
        )

        assert ret["ret"], ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_list_project_region_filter_regex(hub, ctx, gcp_subnetworks):
    if len(gcp_subnetworks) > 0:
        subnetwork = gcp_subnetworks[0]
        subnet_name_substr = subnetwork["name"]
        if len(subnet_name_substr) > 1:
            subnet_name_substr = subnet_name_substr[1:]

        ret = await hub.exec.gcp.compute.subnetwork.list(
            ctx,
            region=_REGION,
            filter=f"name eq '.*{subnetwork['name']}'",
        )

        assert ret["ret"], ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
@pytest.mark.skip(
    reason="Currently creationTimestamp is being filtered from the response, so we cannot check this. "
    "Enable when fixed"
)
async def test_list_project_region_orderby(hub, ctx, gcp_subnetworks):
    if len(gcp_subnetworks) >= 2:
        ret = await hub.exec.gcp.compute.subnetwork.list(
            ctx,
            region=_REGION,
            order_by="creationTimestamp desc",
        )

        assert ret["result"], ret["ret"]
        ordered_subnets = ret["ret"]
        subnet_count = len(ordered_subnets)
        assert subnet_count == len(gcp_subnetworks)
        for i in range(subnet_count - 1):
            assert (
                ordered_subnets[i]["creationTimestamp"]
                >= ordered_subnets[i + 1]["creationTimestamp"]
            )


@pytest.mark.asyncio
async def test_list_project_invalid_region(hub, ctx, gcp_subnetworks):
    ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx,
        region="no-such-region",
    )
    assert not ret["result"]
    assert not ret["ret"]
    assert len(ret["comment"]) > 0


@pytest.mark.asyncio
async def test_list_project_invalid_project(hub, ctx, gcp_subnetworks):
    ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx,
        project="no-such-project",
        region=_REGION,
    )
    assert not ret["result"]
    assert not ret["ret"]
    assert len(ret["comment"]) > 0


@pytest.mark.asyncio
async def test_list_project_invalid_filter(hub, ctx, gcp_subnetworks):
    ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx, region=_REGION, filter="no-such-filter"
    )
    assert not ret["result"]
    assert not ret["ret"]
    assert len(ret["comment"]) > 0


@pytest.mark.asyncio
async def test_list_project_invalid_orderby(hub, ctx, gcp_subnetworks):
    ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx, region=_REGION, order_by="no-such-orderby"
    )
    assert not ret["result"]
    assert not ret["ret"]
    assert len(ret["comment"]) > 0


@pytest.mark.asyncio
async def test_list_project_missing_region(hub, ctx, gcp_subnetworks):
    ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx,
        region=None,
    )
    assert ret["result"], not ret["comment"]
    aggregated_subnetworks = ret.get("ret")
    assert aggregated_subnetworks
    assert len(aggregated_subnetworks) >= len(gcp_subnetworks)


@pytest.mark.asyncio
async def test_list_project_aggregated_with_filter(hub, ctx, gcp_subnetworks):
    ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx, filter=f"region eq .*{_REGION}"
    )
    assert ret["result"], not ret["comment"]
    aggregated_subnetworks = ret.get("ret")
    assert aggregated_subnetworks == gcp_subnetworks


@pytest.mark.asyncio
async def test_get_project_region_name(hub, ctx, gcp_subnetworks):
    if len(gcp_subnetworks) > 0:
        subnetwork = gcp_subnetworks[0]

        ret = await hub.exec.gcp.compute.subnetwork.get(
            ctx,
            region=_REGION,
            name=f"{subnetwork['name']}",
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_subnetwork = ret["ret"]
        assert returned_subnetwork == subnetwork


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_subnetworks):
    if len(gcp_subnetworks) > 0:
        subnetwork = gcp_subnetworks[0]

        ret = await hub.exec.gcp.compute.subnetwork.get(
            ctx,
            resource_id=subnetwork["resource_id"],
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_subnetwork = ret["ret"]
        assert returned_subnetwork == subnetwork


@pytest.mark.asyncio
async def test_get_invalid_project(hub, ctx, gcp_subnetworks):
    name = "some-name"
    ret = await hub.exec.gcp.compute.subnetwork.get(
        ctx,
        project="no-such-project",
        region=_REGION,
        name=name,
    )
    assert ret["result"], not ret["ret"]
    assert f"Get compute.subnetwork '{name}' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_subnetworks):
    invalid_name = "invalid-name"
    ret = await hub.exec.gcp.compute.subnetwork.get(
        ctx,
        region=_REGION,
        name=invalid_name,
    )
    assert ret["result"], not ret["ret"]
    assert f"Get compute.subnetwork '{invalid_name}' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_name_in_resource_id(hub, ctx):
    invalid_resource_id = (
        f"/projects/{ctx.acct.project_id}/regions/{_REGION}/subnetworks/invalid-name"
    )
    ret = await hub.exec.gcp.compute.subnetwork.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert ret["result"], not ret["ret"]
    assert (
        f"Get compute.subnetwork '{invalid_resource_id}' result is empty"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_invalid_project_in_resource_id(hub, ctx):
    invalid_resource_id = (
        f"/projects/no-such-project/regions/{_REGION}/subnetworks/some-name"
    )
    ret = await hub.exec.gcp.compute.subnetwork.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert ret["result"], not ret["ret"]
    assert (
        f"Get compute.subnetwork '{invalid_resource_id}' result is empty"
        in ret["comment"]
    )
