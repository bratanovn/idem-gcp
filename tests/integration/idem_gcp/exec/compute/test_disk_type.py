from typing import Any
from typing import Dict
from typing import List

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module")
async def gcp_disk_types(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.disk_type.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_get_missing_zone_arg(hub, ctx, gcp_disk_types):
    disk_type = next(disk_type for disk_type in gcp_disk_types if disk_type.get("zone"))
    ret = await hub.exec.gcp.compute.disk_type.get(ctx, name=disk_type.get("name"))
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
    assert (
        f'gcp.compute.disk_type {disk_type.get("name")} either resource_id or project, zone and '
        f"name should be specified." in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_missing_name_arg(hub, ctx, gcp_disk_types):
    disk_type = next(disk_type for disk_type in gcp_disk_types if disk_type.get("zone"))
    ret = await hub.exec.gcp.compute.disk_type.get(ctx, zone=disk_type.get("zone"))
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
    assert (
        f"gcp.compute.disk_type None either resource_id or project, zone and "
        f"name should be specified." in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, gcp_disk_types):
    disk_type = next(disk_type for disk_type in gcp_disk_types if disk_type.get("zone"))
    ret = await hub.exec.gcp.compute.disk_type.get(
        ctx,
        zone=disk_type.get("zone"),
        name=disk_type.get("name"),
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert disk_type["resource_id"] == ret["ret"]["resource_id"]
    assert disk_type["name"] in ret["ret"]["name"]
    assert disk_type["zone"] in ret["ret"]["zone"]


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, gcp_disk_types):
    disk_type = next(disk_type for disk_type in gcp_disk_types if disk_type.get("zone"))
    ret = await hub.exec.gcp.compute.disk_type.get(
        ctx, resource_id=disk_type.get("resource_id")
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert disk_type["resource_id"] == ret["ret"]["resource_id"]
    assert disk_type["name"] in ret["ret"]["name"]
    assert disk_type["zone"] in ret["ret"]["zone"]


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_disk_types):
    disk_type = next(disk_type for disk_type in gcp_disk_types if disk_type.get("zone"))
    ret = await hub.exec.gcp.compute.disk_type.get(
        ctx,
        zone=disk_type.get("zone"),
        name="invalid-disk-name",
    )
    assert ret["result"], not ret["ret"]
    assert (
        hub.tool.gcp.comment_utils.get_empty_comment(
            resource_type="compute.disk_type", name=None
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx, gcp_disk_types):
    disk_type = next(disk_type for disk_type in gcp_disk_types if disk_type.get("zone"))
    invalid_resource_id = f"/projects/{ctx.acct.project_id}/zones/{disk_type.get('zone')}/diskTypes/some-name"
    ret = await hub.exec.gcp.compute.disk_type.get(ctx, resource_id=invalid_resource_id)
    assert ret["result"], not ret["ret"]
    assert (
        hub.tool.gcp.comment_utils.get_empty_comment(
            resource_type="compute.disk_type", name=invalid_resource_id
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_list_invalid_project(hub, ctx):
    ret = await hub.exec.gcp.compute.disk_type.list(ctx, project="invalid-project")
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_list_for_project(hub, ctx):
    ret = await hub.exec.gcp.compute.disk_type.list(ctx)
    assert ret["result"], ret["ret"]
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_list_zone(hub, ctx, gcp_disk_types):
    disk_type = next(disk_type for disk_type in gcp_disk_types if disk_type.get("zone"))
    disk_types_random_zone = disk_type.get("zone")
    ret = await hub.exec.gcp.compute.disk_type.list(ctx, zone=disk_types_random_zone)
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    for disk_type in ret["ret"]:
        assert disk_type.get("zone") == disk_types_random_zone


@pytest.mark.asyncio
async def test_list_zone_and_filter(hub, ctx, gcp_disk_types):
    disk_type_random = next(
        disk_type for disk_type in gcp_disk_types if disk_type.get("zone")
    )
    ret = await hub.exec.gcp.compute.disk_type.list(
        ctx,
        zone=disk_type_random.get("zone"),
        filter=f"name eq {disk_type_random.get('name')}",
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) == 1
    list_ret_resource = ret["ret"][0]
    assert disk_type_random["resource_id"] == list_ret_resource["resource_id"]
    assert disk_type_random["name"] in list_ret_resource["name"]
    assert disk_type_random["zone"] in list_ret_resource["zone"]
    assert disk_type_random["kind"] in list_ret_resource["kind"]


@pytest.mark.asyncio
async def test_list_filter(hub, ctx, gcp_disk_types):
    disk_type_random = next(
        disk_type for disk_type in gcp_disk_types if disk_type.get("zone")
    )
    ret = await hub.exec.gcp.compute.disk_type.list(
        ctx,
        filter=f"name eq {disk_type_random.get('name')}",
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    for disk_type in ret["ret"]:
        assert disk_type_random["name"] == disk_type["name"]
        assert disk_type_random["kind"] == disk_type["kind"]


@pytest.mark.asyncio
async def test_list_invalid_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.disk_type.list(ctx, filter=f"name eq unknown")
    assert ret["result"], not ret["ret"]
    assert not ret["comment"]
