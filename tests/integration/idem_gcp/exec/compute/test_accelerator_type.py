from typing import Any
from typing import Dict
from typing import List

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module")
async def gcp_accelerator_types(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.accelerator_type.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_get_missing_zone_arg(hub, ctx, gcp_accelerator_types):
    accelerator_type = gcp_accelerator_types[0]
    ret = await hub.exec.gcp.compute.accelerator_type.get(
        ctx, name=accelerator_type.get("name")
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
    assert (
        f'gcp.compute.accelerator_type {accelerator_type.get("name")} either resource_id or project, zone and '
        f"name should be specified." in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_missing_name_arg(hub, ctx, gcp_accelerator_types):
    accelerator_type = gcp_accelerator_types[0]
    ret = await hub.exec.gcp.compute.accelerator_type.get(
        ctx, zone=accelerator_type.get("zone")
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
    assert (
        f"gcp.compute.accelerator_type None either resource_id or project, zone and "
        f"name should be specified." in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, gcp_accelerator_types):
    accelerator_type = gcp_accelerator_types[0]
    ret = await hub.exec.gcp.compute.accelerator_type.get(
        ctx,
        zone=accelerator_type.get("zone"),
        name=accelerator_type.get("name"),
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert accelerator_type.get("resource_id") == ret["ret"].get("resource_id")
    assert accelerator_type.get("name") in ret["ret"].get("name")
    assert accelerator_type.get("zone") in ret["ret"].get("zone")


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, gcp_accelerator_types):
    accelerator_type = gcp_accelerator_types[0]
    ret = await hub.exec.gcp.compute.accelerator_type.get(
        ctx, resource_id=accelerator_type.get("resource_id")
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert accelerator_type.get("resource_id") == ret["ret"].get("resource_id")
    assert accelerator_type.get("name") in ret["ret"].get("name")
    assert accelerator_type.get("zone") in ret["ret"].get("zone")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_accelerator_types):
    accelerator_type = gcp_accelerator_types[0]
    invalid_name = "invalid-accelerator-name"
    ret = await hub.exec.gcp.compute.accelerator_type.get(
        ctx,
        zone=accelerator_type.get("zone"),
        name=invalid_name,
    )
    assert ret["result"], not ret["ret"]
    assert (
        hub.tool.gcp.comment_utils.get_empty_comment(
            resource_type="compute.accelerator_type", name=None
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx, gcp_accelerator_types):
    accelerator_type = gcp_accelerator_types[0]
    invalid_resource_id = f"/projects/{ctx.acct.project_id}/zones/{accelerator_type.get('zone')}/acceleratorTypes/some-name"
    ret = await hub.exec.gcp.compute.accelerator_type.get(
        ctx, resource_id=invalid_resource_id
    )
    assert ret["result"], not ret["ret"]
    assert (
        hub.tool.gcp.comment_utils.get_empty_comment(
            resource_type="compute.accelerator_type", name=invalid_resource_id
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_list_invalid_project(hub, ctx):
    ret = await hub.exec.gcp.compute.accelerator_type.list(
        ctx, project="invalid-project"
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_list_for_project(hub, ctx):
    ret = await hub.exec.gcp.compute.accelerator_type.list(ctx)
    assert ret["result"], ret["ret"]
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_list_zone(hub, ctx, gcp_accelerator_types):
    accelerator_types_random_zone = gcp_accelerator_types[0].get("zone")
    ret = await hub.exec.gcp.compute.accelerator_type.list(
        ctx, zone=accelerator_types_random_zone
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    for accelerator_type in ret["ret"]:
        assert accelerator_type.get("zone") == accelerator_types_random_zone


@pytest.mark.asyncio
async def test_list_zone_and_filter(hub, ctx, gcp_accelerator_types):
    accelerator_type_random = gcp_accelerator_types[0]
    ret = await hub.exec.gcp.compute.accelerator_type.list(
        ctx,
        zone=accelerator_type_random.get("zone"),
        filter=f"id={accelerator_type_random['id']}",
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) == 1
    list_ret_resource = ret["ret"][0]
    assert accelerator_type_random.get("zone") == list_ret_resource.get("zone")
    assert accelerator_type_random.get("id") == list_ret_resource.get("id")
    assert accelerator_type_random.get("name") == list_ret_resource.get("name")


@pytest.mark.asyncio
async def test_list_filter(hub, ctx, gcp_accelerator_types):
    accelerator_type_random = gcp_accelerator_types[0]
    ret = await hub.exec.gcp.compute.accelerator_type.list(
        ctx, filter=f"id={accelerator_type_random['id']}"
    )
    assert ret["result"], ret["ret"]
    assert not ret["comment"]
    for accelerator_type in ret["ret"]:
        assert accelerator_type_random.get("id") == accelerator_type.get("id")


@pytest.mark.asyncio
async def test_list_invalid_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.accelerator_type.list(
        ctx, filter=f"name eq unknown"
    )
    assert ret["result"], not ret["ret"]
    assert not ret["comment"]
