from typing import Any
from typing import Dict

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module")
async def existing_machine_image(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.machine_image.list(ctx)
    assert ret["result"]
    if len(ret["ret"]) > 0:
        yield next(iter(ret["ret"]))
    else:
        yield None


@pytest.mark.asyncio
async def test_machine_image_list_filter(hub, ctx, existing_machine_image):
    if not existing_machine_image:
        return
    existing_machine_image_name = existing_machine_image["name"]
    ret = await hub.exec.gcp.compute.machine_image.list(
        ctx,
        filter=f"name eq {existing_machine_image_name}",
    )
    assert len(ret["ret"]) == 1
    machine_image = ret["ret"][0]
    assert machine_image["name"] == existing_machine_image_name

    ret = await hub.exec.gcp.compute.machine_image.list(
        ctx,
        filter=f"name ne {existing_machine_image_name}",
    )
    for machine_image in ret["ret"]:
        assert machine_image["name"] != existing_machine_image_name


@pytest.mark.asyncio
async def test_machine_image_get_by_resource_id(hub, ctx, existing_machine_image):
    if not existing_machine_image:
        return
    ret = await hub.exec.gcp.compute.machine_image.get(
        ctx, resource_id=existing_machine_image.get("resource_id")
    )
    machine_image = ret["ret"]
    assert machine_image["name"] == existing_machine_image.get("name")

    ret = await hub.exec.gcp.compute.machine_image.get(
        ctx,
        resource_id="@@##$$%%^^",
    )
    assert not ret["ret"]


@pytest.mark.asyncio
async def test_machine_image_get_by_project_and_name(hub, ctx, existing_machine_image):
    if not existing_machine_image:
        return
    ret = await hub.exec.gcp.compute.machine_image.get(
        ctx,
        machine_image=existing_machine_image.get("name"),
    )
    machine_image = ret["ret"]
    assert machine_image["name"] == existing_machine_image.get("name")

    ret = await hub.exec.gcp.compute.machine_image.get(
        ctx,
        machine_image="@@##$$%%^^",
    )
    assert not ret["ret"]
