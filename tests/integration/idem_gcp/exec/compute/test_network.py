import uuid

import pytest
from pytest_idem import runner

PARAMETER = {
    "network_1_name": "idem-test-network-" + str(uuid.uuid4()),
    "network_2_name": "idem-test-network-" + str(uuid.uuid4()),
    "network_3_name": "idem-test-network-" + str(uuid.uuid4()),
    "network_1_auto_create_subnetworks": False,
    "network_2_auto_create_subnetworks": False,
    "network_3_auto_create_subnetworks": True,
    "peering_1_name": "idem-test-peering-" + str(uuid.uuid4()),
    "peering_2_name": "idem-test-peering-" + str(uuid.uuid4()),
    "subnetwork_1_ip_cidr_range": "10.0.0.0/24",
    "subnetwork_2_ip_cidr_range": "128.0.0.0/24",
}

NETWORK_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - auto_create_subnetworks: {auto_create_subnetworks}
  - peerings: []
  - routing_config:
      routing_mode: REGIONAL
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
"""

NETWORK_WITH_PEERING_SPEC = """
{network_name}:
  gcp.compute.network.present:
  - project: tango-gcp
  - name: {network_name}
  - auto_create_subnetworks: {auto_create_subnetworks}
  - routing_config:
      routing_mode: REGIONAL
  - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
  - peerings:
    - auto_create_routes: true
      exchange_subnet_routes: true
      export_custom_routes: false
      export_subnet_routes_with_public_ip: true
      import_custom_routes: false
      import_subnet_routes_with_public_ip: false
      name: {peering_name}
      network: {peering_network}
      peer_mtu: 1520
"""

SUBNETWORK_SPEC = """
{subnetwork_name}:
  gcp.compute.subnetwork.present:
  - network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/{network_name}
  - ip_cidr_range: {ip_cidr_range}
  - region: us-central1
  - private_ip_google_access: false
  - enable_flow_logs: false
  - private_ipv6_google_access: DISABLE_GOOGLE_ACCESS
  - purpose: PRIVATE
  - stack_type: IPV4_ONLY
"""

NETWORK_ABSENT_SPEC = """
test_network_absent:
  gcp.compute.network.absent:
  - resource_id: {resource_id}
"""

SUBNETWORK_ABSENT_SPEC = """
test_subnetwork_absent:
  gcp.compute.subnetwork.absent:
  - resource_id: {resource_id}
"""


@pytest.fixture
def network_1(hub, idem_cli):
    global PARAMETER

    # Create network
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": PARAMETER["network_1_name"],
                    "auto_create_subnetworks": PARAMETER[
                        "network_1_auto_create_subnetworks"
                    ],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        network_present_ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )

    assert network_present_ret["result"], network_present_ret["comment"]
    resource_id = network_present_ret["new_state"]["resource_id"]
    PARAMETER["network_1_resource_id"] = resource_id

    # Add subnetwork
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            SUBNETWORK_SPEC.format(
                **{
                    "subnetwork_name": PARAMETER["network_1_name"],
                    "network_name": PARAMETER["network_1_name"],
                    "ip_cidr_range": PARAMETER["subnetwork_1_ip_cidr_range"],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        subnetwork_present_ret = next(
            iter(
                [_ for key, _ in state_ret.items() if "gcp.compute.subnetwork_|" in key]
            )
        )

    assert subnetwork_present_ret["result"], subnetwork_present_ret["comment"]
    resource_id = subnetwork_present_ret["new_state"]["resource_id"]
    PARAMETER["subnetwork_1_resource_id"] = resource_id
    return network_present_ret["new_state"]


@pytest.fixture
def network_2(hub, idem_cli):
    global PARAMETER

    # Create network
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_WITH_PEERING_SPEC.format(
                **{
                    "network_name": PARAMETER["network_2_name"],
                    "peering_name": PARAMETER["peering_1_name"],
                    "peering_network": PARAMETER["network_1_resource_id"],
                    "auto_create_subnetworks": PARAMETER[
                        "network_2_auto_create_subnetworks"
                    ],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        network_present_ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )

    assert network_present_ret["result"], network_present_ret["comment"]
    resource_id = network_present_ret["new_state"]["resource_id"]
    PARAMETER["network_2_resource_id"] = resource_id

    # Add subnetwork
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            SUBNETWORK_SPEC.format(
                **{
                    "subnetwork_name": PARAMETER["network_2_name"],
                    "network_name": PARAMETER["network_2_name"],
                    "ip_cidr_range": PARAMETER["subnetwork_2_ip_cidr_range"],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        subnetwork_present_ret = next(
            iter(
                [_ for key, _ in state_ret.items() if "gcp.compute.subnetwork_|" in key]
            )
        )

    assert subnetwork_present_ret["result"], subnetwork_present_ret["comment"]
    resource_id = subnetwork_present_ret["new_state"]["resource_id"]
    PARAMETER["subnetwork_2_resource_id"] = resource_id
    return network_present_ret["new_state"]


@pytest.fixture
def network_1_active_peering(hub, idem_cli, network_1, network_2):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_WITH_PEERING_SPEC.format(
                **{
                    "network_name": PARAMETER["network_1_name"],
                    "peering_name": PARAMETER["peering_2_name"],
                    "peering_network": PARAMETER["network_2_resource_id"],
                    "auto_create_subnetworks": PARAMETER[
                        "network_1_auto_create_subnetworks"
                    ],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        network_present_update_ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )

    assert network_present_update_ret["result"], network_present_update_ret["comment"]

    return network_present_update_ret["new_state"]


@pytest.fixture
def network_3(hub, idem_cli):
    global PARAMETER

    # Create network
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": PARAMETER["network_3_name"],
                    "auto_create_subnetworks": PARAMETER[
                        "network_3_auto_create_subnetworks"
                    ],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        network_present_ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )

    assert network_present_ret["result"], network_present_ret["comment"]
    resource_id = network_present_ret["new_state"]["resource_id"]
    PARAMETER["network_3_resource_id"] = resource_id

    return network_present_ret["new_state"]


@pytest.mark.asyncio
async def test_network_list_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.network.list(ctx, filter="name eq default")
    assert len(ret["ret"]) == 1
    network = ret["ret"][0]
    assert network["name"] == "default"

    ret = await hub.exec.gcp.compute.network.list(ctx, filter="name ne default")
    for network in ret["ret"]:
        assert network["name"] != "default"


@pytest.mark.asyncio
async def test_network_get(hub, ctx):
    ret = await hub.exec.gcp.compute.network.get(ctx, name="default")
    network = ret["ret"]
    assert network["name"] == "default"

    ret = await hub.exec.gcp.compute.network.get(ctx, name="@@##$$%%^^")
    assert not ret["ret"]


# TODO: This test should be enhanced once we add functionality for creating firewalls with idem state
#  Until then, we are testing it with the firewalls for the default network.
@pytest.mark.asyncio
async def test_instance_get_effective_firewalls(hub, ctx, idem_cli):
    ret = await hub.exec.gcp.compute.network.get(ctx, name="default")
    network = ret["ret"]

    ret = await hub.exec.gcp.compute.network.get_effective_firewalls(
        ctx,
        network=network["name"],
    )

    # The 'default' network has 3 firewalls, http, https, ssh.
    assert ret["ret"], ret["result"]
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_network_list_peering_routes(hub, ctx, network_1_active_peering):
    network_peering = network_1_active_peering.get("peerings")[0]
    ret = await hub.exec.gcp.compute.network.list_peering_routes(
        ctx=ctx,
        network=network_1_active_peering["name"],
        peering_name=network_peering["name"],
        region="us-central1",
        direction="OUTGOING",
    )

    assert ret["result"]

    peering_routes = ret["ret"]
    assert peering_routes
    peering_route_item = peering_routes[0]
    assert peering_route_item

    assert peering_route_item["nextHopRegion"] == "us-central1"


async def test_network_switch_to_custom_mode(hub, ctx, network_3):
    assert network_3["auto_create_subnetworks"]
    network_ret = await hub.exec.gcp.compute.network.switch_to_custom_mode(
        ctx=ctx,
        network=network_3["name"],
    )

    assert network_ret["result"]
    assert not network_ret["ret"]["auto_create_subnetworks"]


@pytest.mark.asyncio
async def test_cleanup(hub, ctx, idem_cli):
    # Remove the peerings from the networks using the SPEC with no peerings
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": PARAMETER["network_1_name"],
                    "auto_create_subnetworks": PARAMETER[
                        "network_1_auto_create_subnetworks"
                    ],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        network_present_update_ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )

        assert network_present_update_ret["result"], network_present_update_ret[
            "comment"
        ]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            NETWORK_SPEC.format(
                **{
                    "network_name": PARAMETER["network_2_name"],
                    "auto_create_subnetworks": PARAMETER[
                        "network_2_auto_create_subnetworks"
                    ],
                }
            )
        )
        state_ret = idem_cli(
            "state",
            fh,
            "--acct-profile=test_development_idem_gcp",
            check=True,
        ).json
        network_present_update_ret = next(
            iter([_ for key, _ in state_ret.items() if "gcp.compute.network_|" in key])
        )

        assert network_present_update_ret["result"], network_present_update_ret[
            "comment"
        ]

    # Delete all subnetworks
    subnetwork_list_ret = await hub.exec.gcp.compute.subnetwork.list(
        ctx=ctx, filter=f"name eq {PARAMETER.get('network_3_name')}"
    )
    network_3_subnetwork_resource_ids = [
        subnetwork.get("resource_id") for subnetwork in subnetwork_list_ret["ret"]
    ]
    subnetwork_resource_ids = [
        PARAMETER.get("subnetwork_1_resource_id"),
        PARAMETER.get("subnetwork_2_resource_id"),
    ] + network_3_subnetwork_resource_ids

    for resource_id in subnetwork_resource_ids:
        if resource_id:
            with runner.named_tempfile(suffix=".sls") as fh:
                fh.write_text(
                    SUBNETWORK_ABSENT_SPEC.format(**{"resource_id": resource_id})
                )

                state_ret = idem_cli(
                    "state",
                    fh,
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
                subnetwork_absent_ret = next(
                    iter(
                        [
                            _
                            for key, _ in state_ret.items()
                            if "gcp.compute.subnetwork_|" in key
                        ]
                    )
                )
                assert subnetwork_absent_ret["result"], subnetwork_absent_ret["comment"]
                assert not subnetwork_absent_ret.get("new_state")

    # Delete all networks
    network_resource_ids = [
        PARAMETER.get("network_1_resource_id"),
        PARAMETER.get("network_2_resource_id"),
        PARAMETER.get("network_3_resource_id"),
    ]
    for resource_id in network_resource_ids:
        if resource_id:
            with runner.named_tempfile(suffix=".sls") as fh:
                fh.write_text(
                    NETWORK_ABSENT_SPEC.format(**{"resource_id": resource_id})
                )

                state_ret = idem_cli(
                    "state",
                    fh,
                    "--acct-profile=test_development_idem_gcp",
                    check=True,
                ).json
                network_absent_ret = next(
                    iter(
                        [
                            _
                            for key, _ in state_ret.items()
                            if "gcp.compute.network_|" in key
                        ]
                    )
                )
                assert network_absent_ret["result"], network_absent_ret["comment"]
                assert not network_absent_ret.get("new_state")
