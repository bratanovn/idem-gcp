import pytest


@pytest.mark.asyncio
async def test_reservation_list_filter(hub, ctx):
    all_reservations = await hub.exec.gcp.compute.reservation.list(ctx)
    if len(all_reservations["ret"]) < 1:
        return
    first_reservation = next(iter(all_reservations["ret"]))
    first_reservation_name = first_reservation["name"]
    ret = await hub.exec.gcp.compute.reservation.list(
        ctx,
        zone=first_reservation.get("zone"),
        filter=f"name eq {first_reservation_name}",
    )
    assert len(ret["ret"]) == 1
    reservation = ret["ret"][0]
    assert reservation["name"] == first_reservation_name

    ret = await hub.exec.gcp.compute.reservation.list(
        ctx,
        zone=first_reservation.get("zone"),
        filter=f"name ne {first_reservation_name}",
    )
    for network in ret["ret"]:
        assert network["name"] != first_reservation_name


@pytest.mark.asyncio
async def test_reservation_get(hub, ctx):
    all_reservations = await hub.exec.gcp.compute.reservation.list(ctx)
    if len(all_reservations["ret"]) < 1:
        return
    to_get = next(iter(all_reservations["ret"]))
    ret = await hub.exec.gcp.compute.reservation.get(
        ctx, resource_id=to_get.get("resource_id")
    )
    reservation = ret["ret"]
    assert reservation["name"] == to_get.get("name")

    ret = await hub.exec.gcp.compute.reservation.get(
        ctx,
        zone=to_get.get("zone"),
        reservation="@@##$$%%^^",
    )
    assert not ret["ret"]
