import pytest


@pytest.mark.asyncio
async def test_key_ring_list(hub, ctx):
    location = f"projects/{ctx.acct.project_id}/locations/us-east1"
    ret = await hub.exec.gcp.cloudkms.key_ring.list(ctx, location=location)
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) > 1
    assert f"{location}/keyRings/dvatov-idem-gcp-test-1" in [
        r["resource_id"] for r in ret["ret"]
    ]


@pytest.mark.asyncio
async def test_key_ring_list_empty(hub, ctx):
    location = f"projects/{ctx.acct.project_id}/locations/asia-southeast2"
    ret = await hub.exec.gcp.cloudkms.key_ring.list(ctx, location=location)
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) == 0
    assert len(ret["comment"]) == 1 and ret["comment"][
        0
    ] == hub.tool.gcp.comment_utils.list_empty_comment(
        "gcp.cloudkms.key_ring", location
    ), ret[
        "comment"
    ]


@pytest.mark.asyncio
async def test_key_ring_get(hub, ctx):
    ret = await hub.exec.gcp.cloudkms.key_ring.get(
        ctx,
        resource_id=f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1",
    )
    assert ret["result"], ret["comment"]
    assert (
        ret["ret"]["resource_id"]
        == f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1"
    )


@pytest.mark.asyncio
async def test_key_ring_get_empty(hub, ctx):
    resource_id = (
        f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1000"
    )
    ret = await hub.exec.gcp.cloudkms.key_ring.get(ctx, resource_id=resource_id)
    assert ret["result"], ret["comment"]
    assert len(ret["comment"]) == 1 and ret["comment"][
        0
    ] == hub.tool.gcp.comment_utils.get_empty_comment(
        "gcp.cloudkms.key_ring", resource_id
    ), ret[
        "comment"
    ]
