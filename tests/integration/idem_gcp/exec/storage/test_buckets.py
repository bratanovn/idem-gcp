from typing import Any
from typing import Dict
from typing import List

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module")
async def gcp_buckets(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.storage.buckets.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_get_missing_bucket_name(hub, ctx):
    ret = await hub.exec.gcp.storage.buckets.get(
        ctx,
        name=None,
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
    assert 'Missing required parameter "bucket"' in ret["comment"]


@pytest.mark.skip
@pytest.mark.asyncio
async def test_get_bucket(hub, ctx, gcp_buckets):
    if len(gcp_buckets) > 0:
        bucket = gcp_buckets[0]
        ret = await hub.exec.gcp.storage.buckets.get(
            ctx,
            name=bucket.get("name"),
        )
        assert ret["result"], ret["ret"]
        assert ret["comment"] == []


@pytest.mark.skip
@pytest.mark.asyncio
async def test_get_bucket_full_projection(hub, ctx, gcp_buckets):
    if len(gcp_buckets) > 0:
        bucket = gcp_buckets[0]
        ret = await hub.exec.gcp.storage.buckets.get(
            ctx,
            name=bucket.get("name"),
            projection="full",
        )
        assert ret["result"], ret["ret"]
        assert ret["comment"] == []
        assert ret["ret"]["acl"]


@pytest.mark.asyncio
async def test_list_bucket_filter_by_prefix(hub, ctx, gcp_buckets):
    if len(gcp_buckets) > 0:
        bucket = gcp_buckets[0]

        ret = await hub.exec.gcp.storage.buckets.list(
            ctx,
            prefix=bucket["name"],
        )

        assert ret["ret"], ret["result"]
        assert len(ret["ret"]) >= 1
